package com.telstra.codechallenge.users.accounts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.telstra.codechallenge.users.accounts.dto.AccountResponse;
import com.telstra.codechallenge.users.accounts.dto.AccountsList;

@Service
public class UserAccountServiceImpl implements UserAccountService {
	
	@Autowired private RestTemplate restTemplate;
	private final int STARTING_PAGE =1;
	private final String SORTING_TYPE="joined";
	private final String SORTING_ORDER="asc";
	
	

	
	/**
	 * method to retrieve the recordCount number of user accounts with zero followers
	 * Accepts the default GitHub root URI  and path 
	 * @param query
	 * @param userAccountsDisplayCount
	 * @param githubAPIUri
	 * @param gitHubAPIUsersSearchPath
	 * @return List<AccountResponse> list of account responses  with id, login and html_url
	 */
	@Override
	public List<AccountResponse> getUserAccountsByFollowers(String query,
									String userAccountsDisplayCount,
									String githubAPIUri,String gitHubAPIUsersSearchPath) {
		
			AccountsList userList = new AccountsList();
		
			// Create the encoded URL to access the GitHub API
		    final String baseUrl = githubAPIUri + gitHubAPIUsersSearchPath;
		    UriComponentsBuilder  builder = UriComponentsBuilder.fromUriString(baseUrl)
		    													.queryParam("q", query)
		    													.queryParam("sort", SORTING_TYPE)
		    													.queryParam("order", SORTING_ORDER)
		    													.queryParam("page", STARTING_PAGE)
		    													.queryParam("per_page", Integer.parseInt(userAccountsDisplayCount));
		    UriComponents uriComponents = builder.build().encode();
		    
		    // Create Header and its components for the GitHub API request
		    HttpHeaders headers = new HttpHeaders();
		    headers.set("user-agent", "request");  
		    HttpEntity<?> requestEntity = new HttpEntity<>(null, headers);
		    
		    // Invoke the GitHub search users API
			ResponseEntity<AccountsList> responseEntity = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET,requestEntity, AccountsList.class);
			if (responseEntity.hasBody()) {
				userList = responseEntity.getBody();
			} 
			
		return userList.getItems();
	}
	
}
