package com.telstra.codechallenge.users.accounts.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class AccountsList {
	
	private String total_count;
	private String incomplete_results;
	private List<AccountResponse>  items;

}
