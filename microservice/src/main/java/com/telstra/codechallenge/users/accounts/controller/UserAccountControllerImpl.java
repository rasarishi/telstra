package com.telstra.codechallenge.users.accounts.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.users.accounts.dto.AccountResponse;
import com.telstra.codechallenge.users.accounts.service.UserAccountService;

@RestController
public class UserAccountControllerImpl implements UserAccountController {
	
	@Autowired private UserAccountService userAccountService;
	
	@Value("${githubapi.base.url}")
	private String githubAPIUri;
	
	@Value("${githubapi.user.searchpath}")
	private String gitHubAPIUsersSearchPath;
	
	private final String DEFAULT_RECORD_SIZE = "5";
	private final String DEFAULT_QUERY ="followers:0";
	
	
	@Override
	/**
	 * method to retrieve the recordCount number of user accounts with zero followers
	 * checks for the presence or absence of record Count and assigns default values
	 * Scenarios : 
	 * recordCount <= 0 the service returns default of 5 records
	 * recordCount > 0 the service returns the actual number of records
	 * @param recordCount : no of records to be retrieved
	 * @return List<AccountResponse> list of account responses  with id, login and html_url
	 */
	public List<AccountResponse> getByZeroFollowers(Optional<String> recordCount) {
		
		String noOfRows=recordCount.isPresent() && (Integer.parseInt(recordCount.get()) > 0)? recordCount.get():DEFAULT_RECORD_SIZE;
		
		return userAccountService.getUserAccountsByFollowers(DEFAULT_QUERY,noOfRows,githubAPIUri,gitHubAPIUsersSearchPath );
	}

	
}
