package com.telstra.codechallenge.users.accounts.service;

import java.util.List;

import com.telstra.codechallenge.users.accounts.dto.AccountResponse;


public interface UserAccountService {
	
	/**
	 * method to retrieve the recordCount number of user accounts with zero followers
	 * Accepts the default GitHub root URI  and path 
	 * @param query
	 * @param userAccountsDisplayCount
	 * @param githubAPIUri
	 * @param gitHubAPIUsersSearchPath
	 * @return List<AccountResponse> list of account responses  with id, login and html_url
	 */
	List<AccountResponse> getUserAccountsByFollowers(String query,String userAccountsDisplayCount,
			String githubAPIUri,String gitHubAPIUsersSearchPath);

}
