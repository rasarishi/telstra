package com.telstra.codechallenge.users.accounts.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountResponse {
	private String id;
	private String login;
	private String html_url;

}
