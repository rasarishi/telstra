package com.telstra.codechallenge.users.accounts.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.users.accounts.dto.AccountResponse;

@RestController
public interface UserAccountController {
	
	@GetMapping("/search/users")
	/**
	 * method to retrieve the recordCount number of user accounts with zero followers
	 * @param recordCount : no of records to be retrieved
	 * @return List<AccountResponse> list of account responses  with id, login and html_url
	 */
	List<AccountResponse> getByZeroFollowers(@RequestParam Optional<String> recordCount);

}
