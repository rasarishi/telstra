Feature: Test getByZeroFollowers method in User Account

    Scenario Outline: Get first  few records with least number of followers based on the parameter recordCount 
      When I Send GET request with header and parameters with recordCount <recordCount>
      Then we get successfull response with status code <expectedCode>
      
      Examples:
      | recordCount | expectedCode |
      | 5           | 200          |
      | -5          | 200          |
      | 0           | 200          |
    
    