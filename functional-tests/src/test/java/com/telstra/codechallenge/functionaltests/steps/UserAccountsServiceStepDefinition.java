package com.telstra.codechallenge.functionaltests.steps;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@ContextConfiguration("classpath:cucumber.xml")
public class UserAccountsServiceStepDefinition {

	String baseURL = "http://localhost:8080/search/users?q=followers:0";
	Map<String, ResponseEntity<List>> responseMap = new HashMap<>();

	@Before
	public void beforeScenario() {
		responseMap.clear();
	}

	@When("^I Send GET request with header and parameters with recordCount (\\d+)$")
	public void sendGetRequestWithRecordCount(int recordCount) {
		ResponseEntity<List> response = getAccountListResponseEntity(recordCount);
		responseMap.put("responseEntity", response);
	}

	@When("^I Send GET request with header and parameters with recordCount -(\\d+)$")
	public void sendGetRequestWithNegativeRecordCount(int recordCount) {
		ResponseEntity<List> response = getAccountListResponseEntity(recordCount);
		responseMap.put("responseEntity", response);
	}

	@Then("^we get successfull response with status code (\\d+)$")
	public void theServiceReturnsStatusCode(int expectedCode) {
		assertEquals(expectedCode, responseMap.get("responseEntity").getStatusCodeValue());
	}

	protected ResponseEntity<List> getAccountListResponseEntity(int recordCount) {

		RestTemplate restTemplate = new RestTemplate();

		return restTemplate.exchange(baseURL + "&recordCount=" + recordCount, HttpMethod.GET, null, List.class);

	}

}
